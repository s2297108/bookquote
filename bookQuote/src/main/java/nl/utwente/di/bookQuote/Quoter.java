package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    HashMap<String, Double> books = new HashMap<String, Double>();
    public double getBookPrice(String isbn){
        switch (isbn){
            case "1":
                books.put("1", 10.0);
                break;
            case "2":
                books.put("2", 45.0);
                break;
            case "3":
                books.put("3", 20.0);
                break;
            case "4":
                books.put("4", 35.0);
                break;
            case "5":
                books.put("5", 50.0);
            default:
                books.put("others", 0.0);
        }
        double bookPrice = 0;
        for (Map.Entry<String, Double> entry : books.entrySet()){
            if (isbn.equals(entry.getKey())) {
                bookPrice = entry.getValue();
            }
        }
        return bookPrice;
    }

}
