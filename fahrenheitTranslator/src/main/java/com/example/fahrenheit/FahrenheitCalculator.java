package com.example.fahrenheit;

public class FahrenheitCalculator {

    public double getFahrenheit(String celsius){
        int celsiusInt = Integer.parseInt(celsius);
        return celsiusInt*1.8+32;
    }
}
