package com.example.fahrenheit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class FahrenheitTranslator extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private FahrenheitCalculator calculator;

    public void init(){
        calculator = new FahrenheitCalculator();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Fahrenheit Translator";
        out.println(docType + "<HTML>\n" + "<HEAD><TITLE>" + title + "</TITLE>" + "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" + "<BODY BGCOLOR=\"#FDF5E6\">\n" + "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " + request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " + Double.toString(calculator.getFahrenheit(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }


}
